# Khemistry Next.js application generator.

## Installation
1. Install Yeoman `npm i -g yo`
1. Clone this repo
1. Run `npm i` to install all dependencies
1. Run `npm link` to make this generator globally available

## Usage
1. Create your project directory, e.g.: `~/src/example`
1. Run `yo kh-nexjs` from inside the empty project directory
1. Follow the on-screen prompts
