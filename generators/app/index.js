var Generator = require('yeoman-generator');

module.exports = class extends Generator {
  constructor(args, opts) {
    super(args, opts);

    this._copy = this._copy.bind(this);
  }

  _copy(file) {
    this.fs.copy(this.templatePath(file), file);
  }

  _clone(files) {
    files.forEach(this._copy);
  }

  _kebab(str) {
    return str
      .replace(/([a-z])([A-Z])/g, '$1-$2')
      .replace(/[\s_]+/g, '-')
      .toLowerCase();
  }

  async prompting() {
    this.answers = await this.prompt([
      {
        type: 'input',
        name: 'name',
        message: 'Your project name:',
      },
      {
        type: 'confirm',
        name: 'audit',
        message: 'Run NPM audit on completion?',
        default: true,
      },
      {
        type: 'confirm',
        name: 'start',
        message: 'Start application when finished?',
        default: false,
      }
    ]);
  }

  writing() {
    this.spawnCommandSync('git', ['init']);

    this.fs.writeJSON('package.json', {
      name: this._kebab(this.answers.name),
      version: '0.0.1',
      scripts: {
        test: 'NODE_ENV=test jest',
        dev: 'next',
        build: 'next build',
        start: 'next start',
        storybook: 'start-storybook -p 6006',
      },
      husky: {
        hooks: {
          'pre-commit': 'lint-staged'
        }
      },
      'lint-staged': {
        '*.js': [
          'eslint',
        ]
      }
    });

    this._clone([
      '__tests__/components/layout/.gitkeep',
      '__tests__/components/providers/.gitkeep',
      '__tests__/components/static/.gitkeep',
      '__tests__/library/.gitkeep',
      '__tests__/remote/.gitkeep',
      '.babelrc',
      '.dockerignore',
      '.eslintignore',
      '.eslintrc.json',
      '.gitignore',
      '.storybook/addons.js',
      '.storybook/config.js',
      '.storybook/webpack.config.js',
      'components/layout/.gitkeep',
      'components/providers/.gitkeep',
      'components/static/.gitkeep',
      'Dockerfile',
      'jest.config.js',
      'jest.setup.js',
      'library/mocks/router.js',
      'next.config.js',
      'now.json',
      'pages/_document.js',
      'pages/index.js',
      'remote/.gitkeep',
      'stories/.gitkeep',
    ]);

    this.fs.copyTpl(
      this.templatePath('pages/_app.js'),
      this.destinationPath('pages/_app.js'),
      {
        name: this.answers.name
      }
    );
  }

  install() {
    this.npmInstall([
      '@babel/plugin-proposal-class-properties',
      '@storybook/addon-actions@v4.0.0-rc.1',
      '@storybook/addon-links@v4.0.0-rc.1',
      '@storybook/addons@v4.0.0-rc.1',
      '@storybook/react@v4.0.0-rc.1',
      'acorn-jsx',
      'acorn',
      'babel-core',
      'babel-eslint',
      'babel-jest',
      'babel-plugin-styled-components',
      'babel-runtime',
      'enzyme-adapter-react-16',
      'enzyme',
      'eslint-config-airbnb',
      'eslint-plugin-import',
      'eslint-plugin-jsx-a11y',
      'eslint-plugin-react',
      'eslint',
      'espree',
      'husky@1.0.0-rc.13',
      'jest',
      'lint-staged',
      'next',
      'prop-types',
      'ramda',
      'react-dom',
      'react-test-renderer',
      'react',
      'styled-components',
    ], { 'save': true });
  }

  end() {
      this.spawnCommandSync('git', ['add', '.']);
      this.spawnCommandSync('git', ['commit', '-m', 'Sets up baseline NextJs project']);

      this.answers.audit
        && this.spawnCommandSync('npm', ['audit', 'fix']);

      this.answers.start
        && this.spawnCommandSync('npm', ['run', 'dev']);
  }
};
